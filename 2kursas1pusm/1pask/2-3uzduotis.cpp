#include <iostream>
#include <fstream>
#include <string>
class Stack{
    private:
        int size;
        int top;
        char* values;
    public:
        Stack(int _size=0):size(_size){
            values = new char[size];
            top = -1;
        }
        virtual ~Stack(){
            delete[] values;
        }
        bool isFull();
        bool isEmpty();
        void push(char x);
        char pop();
};
enum Order{Normal,Reverse};
std::string pushAndPop(std::string,bool);
int main(){
    system("chcp 65001>NUL");
    std::cout<<"Įveskite žodį arba sakinį."<<std::endl;
    std::string str;
    std::cin>>str;

    std::string original=pushAndPop(str,Normal);
    std::string reversed=pushAndPop(str,Reverse);

    std::cout<<"orginalus: "<<original<<std::endl;
    std::cout<<"apsuktas:  "<<reversed<<std::endl;

    if(original==reversed)std::cout<<"polindromas"<<std::endl;
    else std::cout<<"nepolindromas"<<std::endl;
}
std::string pushAndPop(std::string str,bool order=Normal){
    Stack *st=new Stack(str.size());
    if      (order==Reverse)for(int i=0;i<(int)str.size();i++)   st->push(str[i]);
    else if (order==Normal) for(int i=str.size()-1;i>=0;i--)st->push(str[i]);
    else{
        std::cerr<<"Error, wrong order"<<std::endl;
        exit(1);
    }
    std::string ret="";
    while(!st->isEmpty())ret+=st->pop();
    return ret;
}
bool Stack::isFull(){
    if(top<size-1) return false;
    else return true;
}
bool Stack::isEmpty(){
    if(top==-1) return true;
    else return false;
}
void Stack::push(char x){
    if(!isFull()){
        top++;
        values[top]=x;
    }
}
char Stack::pop(){
    char retVal;
    if(!isEmpty()){
        retVal=values[top];
        top--;
    }
    return retVal;
}
