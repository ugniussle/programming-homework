
// Stack realization, decimal to binary numbers

#include <iostream>
class Stack{
    private:
        int size;
        int top;
        int* values;
    public:
        Stack(size_t _size=0):size(_size){
            values = new int [size];
            top = -1;
        }
        virtual ~Stack(){
            delete values;
        }
        bool isFull();
        bool isEmpty();
        void push(int x);
        int pop();
        //order 0 - ascending, 1 - descending
        void sort(bool order);
};
int main(){
    int sk;
    std::cout<<"Iveskite dešimtainį skaičių."<<std::endl;
    std::cin>>sk;
    int liekana;
    Stack *s=new Stack(64);
    int temp = sk;
    while(temp!=0){
        liekana=temp%2;
        temp/=2;
        s->push(liekana);
    }
    std::cout<<"Dvejetaine sistema šis skaičius: ";
    while(s->isEmpty()==0){
        std::cout<<s->pop();
    }
    std::cout<<std::endl;
    while(sk!=0){
        liekana=sk%10;
        sk/=10;
        s->push(liekana);
    }
    s->sort(1);
    std::cout<<"surisiuotas: ";
    while(s->isEmpty()==0){
        std::cout<<s->pop();
    }
}
bool Stack::isFull(){
    if(top<size-1)  return false;
    else            return true;

}
bool Stack::isEmpty(){
    if(top==-1) return true;
    else        return false;
}
void Stack::push(int x){
    if(!isFull()){
        top++;
        values[top]=x;
    }
}
int Stack::pop(){
    int retVal = 0;
    if(!isEmpty()){
        retVal=values[top];
        top--;
    }
    return retVal;
}
void Stack::sort(bool order){
    int *arr = new int[size];
    int realSize=0;
    for(int i=0;isEmpty()==0;i++,realSize++) arr[i]=pop();
    for(int i=0;i<realSize;i++){
        for(int j=0;j<realSize-1;j++){
            if(order==true ||arr[j]>arr[i])  std::swap(arr[i],arr[j]);
            if(order==false||arr[j]<arr[i])  std::swap(arr[i],arr[j]);
        }
    }
    for(int i=0;i<realSize;i++) push(arr[i]);
    delete arr;
}