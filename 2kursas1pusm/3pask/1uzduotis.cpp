﻿#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <cmath>

class bTreeNode {
    public:
        std::string key;
        bTreeNode* left;
        bTreeNode* right;
        int count;

        bTreeNode() {
            key = "";
            left = nullptr;
            right = nullptr;
            count = 0;
        }

        bool isEmpty() {
            if (key == "")return 1;
            else return 0;
        }
};

enum Dir { left, right };

struct found {
    bTreeNode* curr, * prev;
    Dir dir;
};

class bTree:bTreeNode {
private:
    bTreeNode* root;
public:
    bTree() {
        root = new bTreeNode();
    }

    void addEl(std::string str);

    bool removeEl(std::string);

    found search(std::string);

    void print() {
        printR(root);
    }

    void printR(bTreeNode* r,int=1);

    int width(bTreeNode*, int);

    int height(bTreeNode*);

    int maxWidth();

    void statistics(bTreeNode*,int&,int&);

    bTreeNode* getRoot() {
        return root;
    }
};
void menu(bTree*);

int main() {
    std::ifstream file("duom1.txt");

    bTree* tree = new bTree();

    while (!file.eof()) {
        std::string str;
        file >> str;

        tree->addEl(str);
    }

    system("chcp 65001>NUL");
    menu(tree);
}
void menu(bTree* tree) {
    while (1) {
        std::cout << "Medžio elementai didėjimo tvarka: " << std::endl;
        tree->print(); std::cout << std::endl;

        std::cout   << "Pasirinkite veiksmą:\n1. Pridėti naują elementą\n"
                    << "2. Pašalinti elementą\n3. Rasti elementą\n"
                    <<"4. Medžio statistika\n5. Baigti darbą" << std::endl;

        int c;
        std::cin >> c;
        if (c == 0)exit(1);

        switch (c) {
            case 1: {         //Pridėti elementą
                std::cout << "Įveskite žodį: ";

                std::string str;
                std::cin >> str;

                if (str == "")break;

                tree->addEl(str);
                break;
            }
            case 2: {        //Ištrinti elementą
                std::cout << "Įveskite raktą: ";

                std::string str;
                std::cin >> str;

                if (tree->removeEl(str)) std::cout << std::endl << "Ištrinimas pavyko." << std::endl;
                else std::cout << std::endl << "Ištrinimas nepavyko." << std::endl << std::endl;

                break;
            }
            case 3: {        //rasti
                std::cout << "Įveskite raktą: ";
                std::string str;
                std::cin >> str;

                bTreeNode* s = tree->search(str).curr;

                if (s == nullptr) std::cout << "Elementas nerastas." << std::endl;
                else std::cout << s->key << " [" << s->count << "]" << std::endl;

                system("pause > NUL");
                break;
            }
            case 5: {
                exit(0);
            }
            case 4: {
                int leaves=0, nodes=0, height=tree->height(tree->getRoot());
                tree->statistics(tree->getRoot(), leaves, nodes);

                std::cout << "Medžio statistika: "<<std::endl;
                std::cout << "Medžio aukštis - " << height << std::endl;
                std::cout << "Medžio didžiausias plotis - " << tree->maxWidth() << std::endl;
                std::cout << "Medžio lapų kiekis - " << leaves << std::endl;
                std::cout << "Medžio mazgų kiekis - " << nodes << std::endl;
                std::cout << "Medžio pilnumas - " << ((float) (nodes + leaves)) / (std::pow(2, height) - 1) * 100 << "%" << std::endl;

                system("pause > NUL");
                break;
            }
        }
    }
}

void bTree::statistics(bTreeNode* r,int& leaves,int& nodes){
    if(r!=nullptr){
        if(r->left == nullptr && r->right == nullptr){
            leaves++;
        }
        else nodes++;

        statistics(r->left,leaves,nodes);
        statistics(r->right,leaves,nodes);
    }
}

int bTree::maxWidth() {
    int h = height(root);
    int x = 0;

    for (int i = 1; i <= h; i++) {
        int w = width(root, i);
        if (w > x)x = w;
    }

    return x;
}

int bTree::height(bTreeNode* iter) {
    if (iter == nullptr)   return 0;
    else {
        int lHeight = height(iter->left);
        int rHeight = height(iter->right);
        if (lHeight > rHeight)  return lHeight + 1;
        else                    return rHeight + 1;
    }
}

int bTree::width(bTreeNode* iter, int level) {
    if (iter == nullptr)return 0;
    if (level == 1)     return 1;
    if (level > 1) return   width(iter->left, level - 1)
                            + width(iter->right, level - 1);
    if (level < 1)    return 0;
    return 0;
}

void bTree::printR(bTreeNode* r,int level) { //didejimo tvarka
    if (r != nullptr) {
        printR(r->left, level + 1);
        std::cout << r->key << " [" << r->count << "] level "<<level<<std::endl;
        printR(r->right, level + 1);
    }
}

bool bTree::removeEl(std::string str) {
    found s = search(str);
    bTreeNode* el = s.curr, *prev = s.prev;
    Dir dir = s.dir;

    if (el == nullptr)return 0;
    else {
        if (el->left == nullptr && el->right == nullptr) {
            delete(el);

            if (prev != nullptr) {
                if (dir == Dir::left) prev->left = nullptr;
                else prev->right = nullptr;
            }

            return 1;
        }
        else {
            bTreeNode* iter = el, * iterPrev = prev;
            Dir iterDir;

            if (iter->left != nullptr) {
                iterDir = Dir::left;
                iterPrev = iter;
                iter = iter->left;

                while (iter->right != nullptr) {
                    iterDir = Dir::right;
                    iterPrev = iter;
                    iter = iter->right;
                }

                if (iterDir == Dir::right)  iterPrev->right = iter->left;
                else                        iterPrev->left = iter->left;

                iter->right = el->right; iter->left = el->left;
                if (root == el) root = iter;
                else {
                    delete(el);
                    if (dir == Dir::left)   prev->left = iter;
                    else                    prev->right = iter;
                }

                return 1;
            }

            else if (iter->right != nullptr) {
                iterDir = Dir::right;
                iterPrev = iter;
                iter = iter->right;

                while (iter->left != nullptr) {
                    iterDir = Dir::left;
                    iterPrev = iter;
                    iter = iter->left;
                }

                if (iterDir == Dir::right)  iterPrev->right = iter->right;
                else                        iterPrev->left = iter->right;

                iter->right = el->right; iter->left = el->left;

                if (root == el) root = iter;
                else {
                    bTreeNode* temp = el;
                    delete(temp);
                    if (dir == Dir::left) prev->left = iter;
                    else prev->right = iter;
                }

                return 1;
            }
        }
    }

    return 0;
}

found bTree::search(std::string str) {
    bTreeNode* iter = root, * prev = nullptr;
    Dir dir = Dir::left;
    while (1) {
        if (iter->key == str) {
            return { iter, prev, dir };
            break;
        }
        else if (iter->key < str) {
            if (iter->right == nullptr) return { nullptr,nullptr,dir };

            prev = iter;
            dir = Dir::right;
            iter = iter->right;
        }
        else if (iter->key > str) {
            if (iter->left == nullptr) return { nullptr,nullptr,dir };

            prev = iter;
            dir = Dir::left;
            iter = iter->left;
        }
    }
}
void bTree::addEl(std::string str) {
    bTreeNode* iter = root;
    while (1) {
        if (iter->isEmpty()) {
            iter->key = str;
            iter->count++;
            break;
        }
        else if (iter->key == str) {
            iter->count++;
            break;
        }
        else if (iter->key < str) {

#ifdef DEBUG
            std::cout << str << " is higher than " << iter->key << std::endl;
            std::cout << "right" << std::endl;
#endif

            if (iter->right == nullptr) {
                bTreeNode* t = new bTreeNode();
                iter->right = t;
            }
            iter = iter->right;
        }
        else if (iter->key > str) {

#ifdef DEBUG
            std::cout << str << " is lower than " << iter->key << std::endl;
            std::cout << "left" << std::endl;
#endif

            if (iter->left == nullptr) {
                bTreeNode* t = new bTreeNode();
                iter->left = t;
            }
            iter = iter->left;
        }
    }
}