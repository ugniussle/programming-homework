#include <iostream>
#include <vector>
#include <utility>
#include <list>
#include <map>

#define limit 10000

struct Edge{
    int source, destination, weight;
};

struct Vertex{
    int vertex, weight, prevVertex;
};




using std::endl;
using std::cout;

class Graph{
    private:
        int n;                                      //Graph size
        int *root;                                  //MST cycle check
        std::vector <Edge> edges;                   //edge list
        std::vector <std::vector<Vertex>> bucket;   //vertex adjacency bucket

    public:
        ~Graph(){
            delete root;
        }

        Graph(int _n, std::vector <Edge> _edges):n(_n), edges(_edges){
            root=new int[n];

            for(int i=0; i<n; i++)root[i] = i;

            bucket = [&]{  //lambda function to turn n and edges to vertex adjacency bucket
                std::vector <std::vector<Vertex>> b;

                for(int i=0;i<n;i++){
                    std::vector<Vertex> list;

                    for(int j=0;j<edges.size();j++){

                        if(edges[j].destination==i){
                            list.push_back({edges[j].source,edges[j].weight});
                        }
                        else if(edges[j].source==i){
                            list.push_back({edges[j].destination,edges[j].weight});
                        }
                    }

                    b.push_back(list);
                }

                return b;
            }();
        }

        std::vector <Vertex> Dijsktra(int source){
            // array of distances from source
            std::vector <Vertex> distances;

            //index is vertex, first value weight, second value is previous vertex
            for(int i=0;i<n;i++)distances.push_back({i,limit,-1});

            distances[source].weight=0;

            std::map <int, bool> unvisited;

            for(int i=0;i<n;i++)unvisited[i]=true;

            while(!unvisited.empty()){
                int smallestWeightVertex = [&]{
                    int smallestWeight = limit, smallestVertex=-1;

                    for(auto it = unvisited.begin(); it != unvisited.end(); ++it){
                        if(distances[it->first].weight < smallestWeight){
                            smallestWeight = distances[it->first].weight;
                            smallestVertex = it->first;
                        }
                    }

                    unvisited.erase(smallestVertex);

                    return smallestVertex;
                }();

                std::vector<Vertex> adjVertexList = bucket[smallestWeightVertex];  //list of adjacent vertices

                for(int i=0;i<adjVertexList.size();i++){
                    int previousWeight = distances[smallestWeightVertex].weight;
                    
                    //currently analyzed vertex
                    Vertex v = adjVertexList[i];

                    if(v.weight + previousWeight < distances[v.vertex].weight){
                        distances[v.vertex].weight=v.weight + previousWeight;
                        
                        distances[v.vertex].prevVertex=smallestWeightVertex;
                    }
                }
            }
            return distances;
        }

        Graph* MSTKruskal(){
            std::vector <Edge> newEdges = edges;

            newEdges = [newEdges]{ //sort edges (ascending)
                std::vector <Edge> e = newEdges;
                for(int i=0;i<e.size();i++)
                    for(int j=0;j<e.size()-1;j++)
                        if(e[i].weight<e[j].weight)std::swap(e[i],e[j]);

                return e;
            }();

            std::vector <Edge> MST;
            for(int i=0;i<newEdges.size();i++){
                Edge smallestWeightEdge = newEdges[i];

                if(MSTcycle(smallestWeightEdge,i,newEdges) == false){
                    MST.push_back(smallestWeightEdge);
                    cout<<"adding edge "<<smallestWeightEdge.source<<" "<<smallestWeightEdge.destination<<endl;
                }
                else{
                    cout<<smallestWeightEdge.source<<" "<<smallestWeightEdge.destination<<" sudaro cikla, nedadam"<<endl;
                }
            }
            return new Graph(n,MST);
        }

        //will the edge loop the MST
        bool MSTcycle(Edge edge,int index, std::vector <Edge> sortedEdges){
            int a=sortedEdges[index].destination;
            int b=sortedEdges[index].source;
            
            if(parent(a) != parent(b)){
                union_find(a, b);
                return false;
            }

            return true;
        }

        int parent(int a){
            while(root[a] != a){
                root[a] = root[root[a]];
                a = root[a];
            }

            return a;
        }

        void union_find(int a, int b){
            int a_parent = parent(a);
            int b_parent = parent(b);

            root[a_parent] = root[b_parent];
        }

        void printGraph();
};

int main(){
    int n=10;  //vertices count

    //std::vector <Edge> edges = {{0,1,50}, {1,6,10},{6,2,5}, {2,0,20}, {3,1,10}, {3,2,60}, {3,4,100},{5,4,1},{5,1,110},{0,6,50},{6,3,50},{2,4,110},{3,5,200}};
    //std::vector <Edge> edges = {{0,1,51}, {0,3,49},{0,6,74}, {1,3,45}, {1,2,55}, {2,3,53}, {2,4,59},{2,5,56},{2,6,64},{3,6,51},{4,5,62},{5,6,42}};
    
    //std::vector <Edge> edges = {{0,1,30}, {0,2,15},{0,3,10}, {1,4,60}, {1,3,25}, {2,5,20}, {2,3,40},{3,6,35},{4,6,20},{5,6,30}};
    //std::vector <Edge> edges = {{1,2,30}, {1,3,15},{1,4,10}, {2,5,60}, {2,4,25}, {3,6,20}, {3,4,40},{4,7,35},{5,7,20},{6,7,30}};
    std::vector <Edge> edges = {{1,2,4}, {1,8,8},{2,3,8}, {2,8,11}, {3,4,7}, {3,6,4}, {3,9,2},{4,5,9},{4,6,14},{5,6,10},{6,7,2},{7,8,1},{7,9,6}};

    Graph *graph = new Graph(n,edges);

    while(true){
        system("cls");
        graph->printGraph();
        cout<<"1 - Dikstros algoritmas, 2 - MST, 3 - baigti darba"<<endl;

        int choice;
        std::cin>>choice;

        switch(choice){
            case 0:
                exit(1);
            case 1:{
                cout<<"pasirinkite pradini mazga (0 - "<<n-1<<")"<<endl;
                
                int source;
                std::cin>>source;

                if(source>n-1)exit(2);

                std::vector <Vertex> vertices = graph->Dijsktra(source);
                
                for(int i=0;i<vertices.size();i++)
                    cout<<"is "<<source<<" i "<<i<<" svoris = "<<vertices[i].weight<<endl;
                
                break;
            }
            case 2:{
                Graph *newGraph = graph->MSTKruskal();
                newGraph->printGraph();
                delete newGraph;
                break;
            }
            case 3:
                exit(0);
            default:
                exit(3);
        }
        cout<<endl;
        system("pause");
    }

    return 0;
}
void Graph::printGraph(){
    cout<<"spausdinamas grafo kibiras"<<endl;
    for(int i=0;i<n;i++){
        cout<<i<<" -> ";

        for(int j=0;j<bucket[i].size();j++){
            cout<<"{"<<bucket[i][j].vertex<<", "<<bucket[i][j].weight<<"}";
            cout<<" ";
        }

        cout<<endl;
    }

    cout<<endl;
}