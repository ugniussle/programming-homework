#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <vector>
using std::setw;
class ListElement{
    public:
        ListElement *next;
        int sk;
        std::string fName;
        std::string lName;
        ListElement(){
            next=nullptr;
        }
        ListElement(std::string _fName,std::string _lName,int _sk):
        sk(_sk),fName(_fName),lName(_lName){
            next=nullptr;
        }
};
class List:ListElement{
    private:
        ListElement *P;
        ListElement *R;
    public:
        List(){
            P=nullptr;
        }
        virtual ~List(){
            ListElement *iter=P;
            while(iter!=nullptr){
                P = iter;
                iter = iter->next;
                delete(P);
            }
        }
        void add(std::string,std::string,int);
        void print();
        void sort(std::string="name");
        double avarage();
        bool remove(int);
        void saveToFile(std::string);

};
void menu(List*);
int main(){
    std::ifstream in("2duom.txt");
    List *l = new List();
    while(!in.eof()){
        int t;
        std::string fName, lName;
        in>>fName>>lName>>t;
        if(fName!="" && lName!="") l->add(fName,lName,t);
    }
    in.close();
    system("chcp 65001>NUL");
    menu(l);
}
void menu(List *list){
    while(1){
        system("cls");
        std::cout<<"Studentų sąrašas: "<<std::endl;
        list->print();
        std::cout<<"Pasirinkite veiksmą:\n1. Pridėti naują studentą\n"
        <<"2. Pašalinti studentą\n3. Surūšiuoti sąrašą\n4. Pažymių vidurkis\n"
        <<"5. Išsaugoti sąrašą\n6. Baigti darbą"<<std::endl;
        int c;
        std::cin>>c;
        if(c==0)exit(1);
        switch(c){
            case 1:{         //Pridėti naują studentą
                std::cout<<"Įveskite studento vardą, pavardę ir pažymį: ";
                std::string fName,lName;
                int grade;
                std::cin>>fName>>lName>>grade;
                if(grade==0)exit(1);
                list->add(fName,lName,grade);
                break;
            }
            case 2:{        //Ištrinti studentą iš sąrašo
                std::cout<<"Įveskite numerį: ";
                int student;
                std::cin>>student;
                student--;
                if(list->remove(student)) std::cout<<std::endl<<"Ištrinimas pavyko."<<std::endl;
                else std::cout<<std::endl<<"Ištrinimas nepavyko."<<std::endl<<std::endl;
                break;
            }
            case 3:{        //surūšiuoti sąrašą
                list->sort();
                break;
            }
            case 4:{        //vidurkis
                std::cout<<"Pažymių vidurkis: "<<list->avarage()<<std::endl;
                std::cout<<"Spauskite [Enter], kad tęsti."<<std::endl;
                system("pause>NUL");
                break;
            }
            case 5:{
                std::string file="2duom.txt";
                list->saveToFile(file);
                break;
            }
            case 6:{
                exit(0);
                break;
            }
        }
    }
}
void List::print(){
    ListElement *iter=P;
    for(int i=1;iter!=nullptr;i++){
        std::cout<<setw(2)<<i<<". "<<std::right<<setw(25)<<iter->fName+' '+iter->lName+' '<<std::right<<setw(2)<<iter->sk<<std::endl;
        iter=iter->next;
    }
    std::cout<<std::endl;
}
void List::add(std::string _fName,std::string _lName,int _sk){
    ListElement *N = new ListElement(_fName,_lName,_sk);
    if(P==nullptr){
        P=N;
        R=P;
    }
    else{
        R->next=N;
        R=R->next;
    }
}
bool List::remove(int n){
    ListElement *iter = P,*prev;
    for(int i=0;i<n;i++){
        prev=iter;
        iter=iter->next;
        if(iter==nullptr)return 0;
    }
    if(iter==nullptr)return 0;
    if(n==0) P=iter->next;
    else prev->next=iter->next;
    delete(iter);
    return 1;
}
struct Student{
    std::string fName,lName;
    int grade;
};
void List::sort(std::string type){
    std::vector <Student> s;
    ListElement *iter=P;
    while(iter!=nullptr){
        s.push_back({iter->fName,iter->lName,iter->sk});
        iter=iter->next;
    }
    if(type=="name"){
        for(size_t i=0;i<s.size();i++){
            for(size_t j=0;j<s.size()-1;j++){
                std::string name1 = s[j].lName+s[j].fName+std::to_string(s[j].grade);
                std::string name2 = s[j+1].lName+s[j+1].fName+std::to_string(s[j+1].grade);
                //std::cout<<"comparing "<<name1<<' '<<name2<<' ';
                int cmp = name1.compare(name2);
                //std::cout<<cmp<<std::endl;
                if(cmp>0){
                    //std::cout<<"swapping "<<name1<<' '<<name2<<std::endl;
                    Student temp=s[j];
                    s[j]=s[j+1];
                    s[j+1]=temp;
                }
            }
        }
    }
    iter=P;
    for(int i=0;iter!=nullptr;i++){
        iter->fName=s[i].fName;
        iter->lName=s[i].lName;
        iter->sk=s[i].grade;
        iter=iter->next;
    }
}
double List::avarage(){
    ListElement *iter=P;
    double sum=0;
    int n=0;
    while(iter!=nullptr){
        sum+=iter->sk;
        iter=iter->next;
        n++;
    }
    return sum/n;
}
void List::saveToFile(std::string file){
    std::ofstream out(file.c_str());
    ListElement *iter=P;
    while(iter!=nullptr){
        out<<iter->fName<<' '<<iter->lName<<' '<<iter->sk<<std::endl;
        iter=iter->next;
    }
    out.close();
}
