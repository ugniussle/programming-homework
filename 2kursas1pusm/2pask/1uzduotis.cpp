#include <iostream>
#include <fstream>
#include <iomanip>
class ListElement{
    public:
        ListElement *next;
        int sk;
        ListElement(){
            next=nullptr;
        }
        ListElement(int _sk){
            sk=_sk;
            next=nullptr;
        }
        ~ListElement(){
            if(next!=nullptr)delete(next);
        }
};
class List:ListElement{
    private:
        ListElement *P;
        ListElement *R;
    public:
        List(){
            P=nullptr;
        }
        virtual ~List(){
            ListElement *iter=P;
            while(iter!=nullptr){
                P = iter;
                iter = iter->next;
                delete(P);
            }
        }
        void add(int _sk);
        void print();
        double avarage();

};

int main(){
    std::ifstream in("1duom.txt");
    List *l = new List();
    while(!in.eof()){
        int t;
        in>>t;
        l->add(t);
    }
    in.close();
    std::cout<<"Studentu pazymiu sarasas: "<<std::endl;
    l->print();
    std::cout<<"Pazymiu vidurkis = "<<std::fixed<<std::setprecision(2)<<l->avarage()<<std::endl;
}
void List::print(){
    ListElement *iter=P;
    while(iter!=nullptr){
        std::cout<<iter->sk<<" ";
        iter=iter->next;
    }
    std::cout<<std::endl;
}
void List::add(int _sk){
    ListElement *N = new ListElement(_sk);
    if(P==nullptr){
        P=N;
        R=P;
    }
    else{
        R->next=N;
        R=R->next;
    }
}
double List::avarage(){
    ListElement *iter=P;
    double sum=0;
    int n=0;
    while(iter!=nullptr){
        sum+=iter->sk;
        iter=iter->next;
        n++;
    }
    return sum/n;
}