#include <iostream>
#include <vector>
#include <list>
using std::endl;
using std::cout;
struct Edge{
    int source, destination, weight;
};
struct Vertex{
    int vertex, weight, prevVertex;
};
class Graph{
    private:
        int n;                                      //Graph size
        std::vector <Edge> edges;                   //edge list
        std::vector <std::vector<Vertex>> bucket;   //vertex adjacency bucket
        bool *DFSvisited = new bool[n];

    public:
        Graph(int _n, std::vector <Edge> _edges):n(_n),edges(_edges){
            bucket = [&]{  //lambda function to turn n and edges to vertex adjacency bucket
                std::vector <std::vector<Vertex>> b;
                for(int i=0;i<n;i++){
                    std::vector<Vertex> list;
                    for(int j=0;j<edges.size();j++){
                        if(edges[j].destination==i){
                            list.push_back({edges[j].source,edges[j].weight});
                        }
                        else if(edges[j].source==i){
                            list.push_back({edges[j].destination,edges[j].weight});
                        }
                    }
                    b.push_back(list);
                }
                return b;
            }();
        }
        void BFS(int source){
            bool *visited = new bool[n];
            for(int i=0;i<n;i++) visited[i]=false;
            std::list <int> queue;
            visited[source]=true;
            queue.push_back(source);
            while(!queue.empty()){
                int v = queue.front();
                cout<<v<<" ";
                queue.pop_front();
                for(int i=0;i<bucket[v].size();i++){
                    if(!visited[bucket[v][i].vertex]){
                        visited[bucket[v][i].vertex]=true;
                        queue.push_back(bucket[v][i].vertex);
                    }
                }
            }
            cout<<endl;
        }
        void DFS(int source){
            DFSvisited[source]=true;
            cout<<source<<" ";
            for(int i=0;i<bucket[source].size();i++){
                if(!DFSvisited[bucket[source][i].vertex])
                    DFS(bucket[source][i].vertex);
            }
        }
        void clearVisited(){
            for(int i=0;i<n;i++)DFSvisited[i]=false;
        }
        
        void printGraph();
};

int main(){
    int n=6;  //vertices count
    std::vector <Edge> edges = {{0,1,50}, {1,2,10}, {2,0,20}, {3,1,10}, {3,2,60}, {3,4,100},{5,4,1},{5,1,110}};
    Graph *graph = new Graph(n,edges);
    while(true){
        system("cls");
        graph->printGraph();
        cout<<"1 - BFS, 2 - DFS, 3 - baigti darba"<<endl;

        int choice;
        std::cin>>choice;
        cout<<"pasirinkite pradini mazga (0 - "<<n-1<<")"<<endl;

        int source;
        std::cin>>source;
        if(source>n-1)exit(2);

        switch(choice){
            case 0:
                exit(1);
            case 1:
                graph->BFS(source);
                break;
            case 2:
                graph->DFS(source);
                graph->clearVisited();
                break;
            case 3:
                exit(0);
            default:
                exit(3);
        }
        cout<<endl;
        system("pause");
    }
    return 0;
}
void Graph::printGraph(){
    cout<<"printing bucket"<<endl;
    for(int i=0;i<n;i++){
        cout<<i<<" -> ";
        for(int j=0;j<bucket[i].size();j++){
            cout<<"{"<<bucket[i][j].vertex<<", "<<bucket[i][j].weight<<"}";
            cout<<" ";
        }
        cout<<endl;
    }
    cout<<endl;
}