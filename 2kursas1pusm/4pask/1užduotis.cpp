#include <iostream>
#include <string>
#include <iomanip>

class HashTableElement{
    private:
    std::string key,value;
    public:
    HashTableElement *next;
    HashTableElement(){
        next=nullptr;
        key="";
        value="";
    }
    std::string getUserName(){return key;}
    std::string getPassword(){return value;}
    void setUserName(std::string str){key=str;}
    void setPassword(std::string str){value=str;}
};
struct found{
    HashTableElement *curr,*prev;
};

class HashTable{
    private:
    HashTableElement *table;
    int mod;  //hash modulus and table size
    int elCount;
    public:
    HashTable(size_t _mod):mod(_mod){
        table=new HashTableElement[mod];
        elCount=0;
    }
    bool addElement(std::string,std::string);
    bool removeElement(std::string);
    found find(std::string);
    int size(){return elCount;}
    size_t hashStringtoInt(std::string);
    void print();
};

int main(){
    HashTable *table=new HashTable(20);
    for(int i=0;i<15;i++){
        std::string key="user"+std::to_string(i),value="password"+std::to_string(i);
        table->addElement(key,value);
    }
    while(1){
        std::string key,value;
        //system("cls");
        table->print();
        std::cout<<"vartotoju kiekis: "<<table->size()<<std::endl;
        std::cout<<"ka norite daryti?(1 - registruotis, 2 - prisijungti, 3 - istrinti)"<<std::endl;
        size_t choice=0;
        std::cin>>choice;
        if(choice==0)return 0;
        switch (choice){
            case 1:{
                std::cout<<"iveskite slapyvardi: ";
                std::cin>>key;
                std::cout<<"iveskite slaptazodi: ";
                std::cin>>value;
                table->addElement(key,value);
                break;
            }
            case 2:{
                std::cout<<"iveskite slapyvardi: ";
                std::cin>>key;
                std::cout<<"iveskite slaptazodi: ";
                std::cin>>value;
                found f=table->find(key);
                if(f.curr==nullptr || f.curr->getPassword()!=value)std::cout<<"tokio vartotojo nera, bandykite dar karta"<<std::endl;
                else{
                    std::cout<<std::endl<<"Sekmingai prisijungete"<<std::endl;
                    return 0;
                }
                break;
            }
            case 3:{
                std::cout<<"iveskite slapyvardi: ";
                std::cin>>key;
                if(table->removeElement(key))std::cout<<"istrinta sekmingai"<<std::endl;
                else std::cout<<"vartotojas nerastas"<<std::endl;
                break;
            }
        }
        
    }
}
bool HashTable::addElement(std::string key,std::string value){
    int hash = hashStringtoInt(key);
    if(find(key).curr!=nullptr){
        return 0;
    }
    HashTableElement* iter=&table[hash];
    while(iter->getUserName()!=""){
        if(iter->next==nullptr){
            iter->next=new HashTableElement();
        }
        iter=iter->next;
    }
    iter->setUserName(key);
    iter->setPassword(value);
    elCount++;
    return 1;
}
size_t HashTable::hashStringtoInt(std::string str){
    return std::hash <std::string>{}(str) % mod;
};
void HashTable::print(){
    HashTableElement *iter=table;
    std::cout<<std::left<<std::setw(20)<<"Slapyvardis"<<std::right<<std::setw(20)<<"Slaptazodis"<<std::endl;
    for(int i=0;i<40;i++)std::cout<<"-";
    std::cout<<std::endl;
    for(int i=0;i<mod;i++){
        HashTableElement *iter=&table[i];
        while(iter->getUserName()!=""){
            std::cout<<std::left<<std::setw(20)<<iter->getUserName()<<std::right<<std::setw(20)<<iter->getPassword()<<std::endl;
            if(iter->next==nullptr)break;
            iter=iter->next;
        }
    }
}
found HashTable::find(std::string key){
    int hash = hashStringtoInt(key);
    HashTableElement *iter=&table[hash],*prev=nullptr;
    if(iter==nullptr||iter->getUserName()=="")return { nullptr,nullptr };
    else{
        while(iter->getUserName()!=""){
            if(iter->getUserName()==key)return { iter,prev };
            if(iter->next==nullptr)break;
            prev=iter;
            iter=iter->next;
        }
        return { nullptr,nullptr };
    }
}

bool HashTable::removeElement(std::string key){
    found f = find(key);
    HashTableElement *prev=f.prev, *el=f.curr;
    int mod = hashStringtoInt(key);
    if(el!=nullptr){
        if(prev==nullptr && el->next==nullptr){
            el->setUserName("");
            el->setPassword("");
            elCount--;
            return 1;
        }
        else if(prev==nullptr && el->next!=nullptr){
            table[mod]=*el->next;
            delete(el);
            elCount--;
            return 1;
        }
        else if(prev!=nullptr && el->next==nullptr){
            prev->next=el->next;
            delete(el);
            elCount--;
            return 1;
        }
        else if(prev!=nullptr && el->next!=nullptr){
            prev->next=el->next;
            delete(el);
            elCount--;
            return 1;
        }
    }
    else return 0;
}