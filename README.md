# 2kursas1pusm

## 1 pask

Stack (decimal to binary, palindrome check)

## 2 pask

List (avarage, data management)

## 3 pask

Binary tree (add, remove elements, tree statistics)

## 4 pask

Hash table (add, remove elements, login)

## 5 pask

Graph (BFS, DFS)

## 6 pask

Graph (Djisktra algorithm, MST Kruskal algorithm)