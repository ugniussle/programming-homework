#include <iostream>
using namespace std;
#define Pi 3.14159
#define RoLedo 0.9

const float pi = 3.14159;
const float roledo = 0.9;

double cilMase(int spindulys,int ilgis);

int main(){
    int r,l;
    cout<<"Iveskite spinduli ir ilgi(cm): ";
    cin>>r>>l;
    cout<<cilMase(r,l)<<'g'<<endl;
    system("pause");
    return 0;
}
double cilMase(int spindulys,int ilgis){
    double turis=spindulys*spindulys*Pi*ilgis;
    return turis*roledo;
}