#include <iostream>
using namespace std;
const double peda=0.3048;
const double mylia=1609.344;
void Convert(double m,int &my,double &p);
int main(){
    cout<<"Iveskite metrus: ";
    double metrai,pedos=0;
    int mylios=0;
    cin>>metrai;
    Convert(metrai,mylios,pedos);
    cout<<"Mylios:\t"<<mylios<<"\nPedos:\t"<<pedos<<endl;
    system("pause");
    return 0;
}
void Convert(double m,int &my,double &p){ 
    while(m>=mylia){
        m-=mylia;
        my++;
    }
    p=m/peda;
}