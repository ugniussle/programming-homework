#include <iostream>
#include <cmath>
using namespace std;

const float Pi=3.14159;

double plotas(double r);
void compare(double a, double b, double c);
int main(){
    double a,b,c;
    cout<<"Inveskite 3 skrituliu spindulius: "<<endl;
    cin>>a>>b>>c;
    a=plotas(a);
    b=plotas(b);
    c=plotas(c);
    compare(a,b,c);
    system("pause");
    return 0;
}
void compare(double a, double b, double c){
    double max1,max2;
    if(a>b){
        max1=a;
        if(b>c)max2=b;
        else max2=c;
    }
    else {
        max1=b;
        if(a>c)max2=a;
        else max2=c;
    }
    cout<<"didesni plotai: "<<max1<<' '<<max2<<endl;
}
double plotas(double r){
    return r*r*Pi;
}