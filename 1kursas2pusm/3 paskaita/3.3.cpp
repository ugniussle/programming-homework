#include <iostream>
#include <cmath>
using namespace std;
const float Pi=3.141592;
////////////////
class Taskas{
private:
	int X,Y;
public:
	void ivestKoord(int x,int y){X=x;Y=y;};
	int gautiX(){return X;}
	int gautiY(){return Y;}
};
////////////////
class Atkarpa{
private:
	Taskas Pradzia;
	Taskas Pabaiga;
public:
    Atkarpa();
	float GautiAtkarpa();
};
Atkarpa::Atkarpa(){
	cout<<"Iveskite pirmo tasko koordinates: ";
	int x,y;
	cin>>x>>y;
	Pradzia.ivestKoord(x,y);
	cout<<"Iveskite antro tasko koordinates: ";
	cin>>x>>y;
	Pabaiga.ivestKoord(x,y);
	 cout<<endl;
}
float Atkarpa::GautiAtkarpa(){
	float atkarpa=sqrt(pow((Pradzia.gautiX()-Pabaiga.gautiX()),2)+
	pow((Pradzia.gautiY()-Pabaiga.gautiY()),2));
	return atkarpa;
}
////////////////
class Skritulys{
private:
    Atkarpa R;
public:
    float gautiPlota(){return pow(R.GautiAtkarpa(),2)*Pi;}
};
////////////////
class Kugis{
private:
    Skritulys S1;
    Skritulys S2;
    Atkarpa H;
public:
    float Turis();
};
float Kugis::Turis(){
    return ((S1.gautiPlota()+sqrt(S1.gautiPlota()+S2.gautiPlota())+S2.gautiPlota())/3)*H.GautiAtkarpa();
}
////////////////
int main(){
    float r1,r2,h;
    cout<<"Iveskite pagrindu spinduliu ir aukstines koordinates:\n";
    //cin>>r1>>r2>>h;
    Kugis K1;
    cout<<"Turis="<<K1.Turis()<<endl;
    return 0;
}