#include <iostream>
using namespace std;
class Atlyginimas{
private:
    float antPopieriaus;
    float iRankas;
    float darbdavioMokest;
    float darbuotojoSodrMokest;
    float darbuotojoPajMokest;
public:
    Atlyginimas(float atl){antPopieriaus=atl;};
    float gautiPop(){return antPopieriaus;}
    float gautiRank(){return iRankas;}
    float gautiDarbdMokest(){return darbdavioMokest;}
    float gautiDarbuotSodrMokest(){return darbuotojoSodrMokest;}
    float gautiDarbuotPajMokest(){return darbuotojoPajMokest;}
    //void priskirtiPradAtlyginima(float atl){antPopieriaus=atl;}
    void skaicAtlyginima();
};

class Darbuotojas{
private:
    int amzius;
    int patirtis;
    Atlyginimas A;
public:
    Darbuotojas(int amz,int pat,float atl):amzius(amz),patirtis(pat),A(atl){A.skaicAtlyginima();};
    void Isvedimas();
};

void Atlyginimas::skaicAtlyginima(){
    darbdavioMokest=antPopieriaus*0.3117;
    iRankas=antPopieriaus-darbdavioMokest;
    darbuotojoPajMokest=iRankas*0.15;
    darbuotojoSodrMokest=iRankas*0.09;
    iRankas-=(darbuotojoPajMokest+darbuotojoSodrMokest);
}
/*
Darbuotojas::Darbuotojas(int Amzius,int Patirtis,float Atlyginimas){
    amzius=Amzius;
    patirtis=Patirtis;
    atlyginimas.priskirtiPradAtlyginima(Atlyginimas);
    
}*/

void Darbuotojas::Isvedimas(){
    cout<<"\nDarbuotojo amzius: "<<amzius<<
    " m.\nDarbuotojo patirtis: "<<patirtis<<
    " m.\nDarbuotojo atlygininmas ant popieriaus: "<<A.gautiPop()<<
    " Eur\nDarbuotojo atlygininmas i rankas: "<<A.gautiRank()<<
    " Eur\nDarbdavio sodros mokestis: "<<A.gautiDarbdMokest()<<
    " Eur\nDarbuotojo sodros mokestis: "<<A.gautiDarbuotSodrMokest()<<
    " Eur\nDarbuotojo pajamu mokestis: "<<A.gautiDarbuotPajMokest()<<" Eur\n";
}

int main(){
	cout<<"Iveskite darbuotojo amziu, patirti ir atlyginima:";
	int amzius,patirtis,atlyginimas;
	cin>>amzius>>patirtis>>atlyginimas;
    Darbuotojas Tomas(amzius,patirtis,atlyginimas);
    Tomas.Isvedimas();
    return 0;
}