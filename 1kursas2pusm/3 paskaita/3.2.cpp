#include <iostream>
#include <cmath>
using namespace std;
const float Pi=3.141592;
class Skritulys{
private:
    float spind;
    float plotas;
public:
    void ivestSpind(float r){spind=r;plotas=spind*spind*Pi;}
    float gautiPlota(){return plotas;}
};
class Kugis{
private:
    Skritulys S1;
    Skritulys S2;
    float H;
public:
    float Turis();
    Kugis(float spind1,float spind2,float aukst);
};
Kugis::Kugis(float spind1,float spind2,float aukst){
    S1.ivestSpind(spind1);
    S2.ivestSpind(spind2);
    H=aukst;
}
float Kugis::Turis(){
    return ((S1.gautiPlota()+sqrt(S1.gautiPlota()+S2.gautiPlota())+S2.gautiPlota())/3)*H;
}
int main(){
    float r1,r2,h;
    cout<<"Iveskite pagrindu spindulius ir aukstine: ";
    cin>>r1>>r2>>h;
    Kugis K1(r1,r2,h);
    cout<<"Turis="<<K1.Turis()<<endl;
    return 0;
}