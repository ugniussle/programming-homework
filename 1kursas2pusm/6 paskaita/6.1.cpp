#include <iostream>
using namespace std;
template <typename a,typename b> class staciakampis{
    private:
        a ilgis;
        b plotis;
        b perimetras;
    public:
        staciakampis(a r1,b r2):ilgis(r1),plotis(r2){perimetras=ilgis*2+plotis*2;}
        ~staciakampis(){cout<<ilgis<<' '<<plotis<<' '<<perimetras<<endl;}
};
int main(){
    staciakampis<int,int> sveikas(10,5);
    staciakampis<float,float> realus(5.5,10.12544);
    staciakampis<int,float> sveikas_ir_realus(10,4.75);
    return 0;
}