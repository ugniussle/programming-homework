#include <iostream>
using namespace std;
class Darbuotojas{
    private:
        int amzius;
        int patirtis;
        int atlyginimas;
    public:
        Darbuotojas(int am,int pat,int atl):amzius(am),patirtis(pat),atlyginimas(atl){
            if(amzius>70||amzius<18){throw Amzius();}
            if(amzius-patirtis<18){throw Patirtis();}
            if(atlyginimas<300){throw Atlyginimas();}
        }
        class Amzius{};
        class Patirtis{};
        class Atlyginimas{};
};
int main(){
    int count=0;
    try{
        cout<<"Iveskite amziu, patirti, atlyginima: ";
        int amzius,patirtis,atlyginimas;
        cin>>amzius>>patirtis>>atlyginimas;
        Darbuotojas a(amzius,patirtis,atlyginimas);
    }
    catch(Darbuotojas::Amzius){
        cerr<<"Netinkamas amzius (18-70)."<<endl;
        count++;
    }
    catch(Darbuotojas::Patirtis){
        cerr<<"Netinkama patirtis (per didele ivestam amziui)."<<endl;
        count++;
    }
    catch(Darbuotojas::Atlyginimas){
        cerr<<"Per mazas atlyginimas (nuo 300)."<<endl;
        count++;
    }
    if(count==0)cout<<"Duomenys teisingi.";
}