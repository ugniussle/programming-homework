#include <iostream>
using namespace std;
class Darbuotojas{
    private:
        string vardas;
        string pavarde;
        string specialybe;
        int amzius;
        int patirtis;
        int atlyginimas;
    public:
        void ivestiVarda(){cout<<"Iveskite darbuotojo varda ir pavarde: ";  cin>>vardas>>pavarde;}
        void ivestiSpec(){cout<<"Iveskite darbuotojo specialybe: ";         cin>>specialybe;}
        void ivestiAmziu(){cout<<"Iveskite darbuotojo amziu: ";             cin>>amzius;}
        void ivestiPatirti(){cout<<"Iveskite darbuotojo patirti: ";         cin>>patirtis;}
        void ivestiAtl(){cout<<"Iveskite darbuotojo atlyginima: ";          cin>>atlyginimas;}
        void gautiVarda(){cout<<"Vardas: "<<vardas<<' '<<pavarde<<endl;}
        void gautiSpec(){cout<<"Specialybe: "<<specialybe<<endl;}
        void gautiAmziu(){cout<<"Amzius: "<<amzius<<endl;}
        void gautiPatirti(){cout<<"Patirtis: "<<patirtis<<endl;}
        void gautiAtl(){cout<<"Atlyginimas: "<<atlyginimas<<endl;}
};
int main(){
    void (Darbuotojas::*funkcRod)();
    Darbuotojas *a;
    bool loop=1;
    cout<<"Iveskite skaiciu ivesti atitinkamai informacijai apie darbuotoja.\n";
    while(loop){
        cout<<"\n1 - vardas ir pavarde. 2 - specialybe. 3 - amzius. 4 - patirtis. 5 - atlyginimas. 0 - iseiti.\n";
        int pas;
        cin>>pas;
        switch(pas){
            case 1: funkcRod=&Darbuotojas::ivestiVarda;break;
            case 2: funkcRod=&Darbuotojas::ivestiSpec;break;
            case 3: funkcRod=&Darbuotojas::ivestiAmziu;break;
            case 4: funkcRod=&Darbuotojas::ivestiPatirti;break;
            case 5: funkcRod=&Darbuotojas::ivestiAtl;break;
            default: loop=false; break;
        }
        if(loop) (a->*funkcRod)();
    }
    loop=1;
    cout<<"\n\n\nIveskite skaiciu pamatyti atitinkama informacija apie darbuotoja.\n";
    while(loop){
        cout<<"\n1 - vardas ir pavarde. 2 - specialybe. 3 - amzius. 4 - patirtis. 5 - atlyginimas. 0 - iseiti.\n";
        int pas;
        cin>>pas;
        switch(pas){
            case 1: funkcRod=&Darbuotojas::gautiVarda;break;
            case 2: funkcRod=&Darbuotojas::gautiSpec;break;
            case 3: funkcRod=&Darbuotojas::gautiAmziu;break;
            case 4: funkcRod=&Darbuotojas::gautiPatirti;break;
            case 5: funkcRod=&Darbuotojas::gautiAtl;break;
            default: loop=false; break;
        }
        if(loop) (a->*funkcRod)();
    }
    return 0;
}