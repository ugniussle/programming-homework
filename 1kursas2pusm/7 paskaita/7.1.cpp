#include <iostream>
using namespace std;
class Preke{
    private:
        string pavadinimas;
        int kiekis;
        float vntKaina;
        float visaKaina;
        static int prekiuKiekis;
    public:
        Preke(string pav,int kiek,float kaina):pavadinimas(pav),kiekis(kiek),vntKaina(kaina){
            visaKaina=vntKaina*(float)kiekis;
            prekiuKiekis+=kiekis;
        }
        ~Preke(){
            cout<<"prekiu kiekis: "<<prekiuKiekis<<endl;
            prekiuKiekis-=kiekis;
        }
};
int Preke::prekiuKiekis=0;
int main(){
    Preke *prekes[5];
    for(int i=0;i<5;i++){
        prekes[i]=new Preke("bananas",i+1,i+1.5);
    }
    for(int i=0;i<5;i++){
        delete prekes[i];
    }
    return 0;
}