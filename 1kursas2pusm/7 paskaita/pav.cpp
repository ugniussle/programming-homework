#include <cstdlib>
#include <iostream>
#include <time.h>
using namespace std;
class Darbuotojas{
    private:
        string vardas;
        int amzius;
        int patirtis;
        float atlyginimas;
    public:
        Darbuotojas(string v, int a, int p, float at):vardas(v),amzius(a),patirtis(p),atlyginimas(at){}
        ~Darbuotojas(){}
        void GautiVarda(){cout<<"Vardas: "<<vardas<<"\n";}
        void GautiAmziu(){cout<<"Amzius: "<<amzius<<"\n";}
        void GautiPatirti(){cout<<"Patirtis: "<<patirtis<<"\n";}
        void GautiAtlyginima(){cout<<"Atlyginimas: "<<atlyginimas<<"\n";}
};
int main(int argc, char *argv[]){
    void (Darbuotojas::*fRod)();
    Darbuotojas *personalas=0;
    personalas = new Darbuotojas("Jonas", 44,25,1111);
    int pabaiga=0, pasirinkimas;
    while (!pabaiga){
        cout<<"Ka norite suzinoti apie darbuotoja?\n";
        cout<<"(0) Uzbaigti, (1)Vardas, (2) Amzius, (3) Patirtis, (4) Atlyginimas\n";
        cin>>pasirinkimas;
        switch(pasirinkimas){
            case 0: pabaiga=1; break;
            case 1: fRod=&Darbuotojas::GautiVarda; break;
            case 2: fRod=&Darbuotojas::GautiAmziu; break;
            case 3: fRod=&Darbuotojas::GautiPatirti; break;
            case 4: fRod=&Darbuotojas::GautiAtlyginima; break;
            default : return 0;
        }
        (personalas->*fRod)();
    }
    system("pause");
    return EXIT_SUCCESS;
}