#include <iostream>
#include <cmath>
using namespace std;

class Funkcija{
    protected:
    float x;

    public:
    Funkcija(float X):x(X){}
    virtual ~Funkcija(){}
    virtual float reiksme()=0;
};

class Tiesine:public Funkcija{
private:
    float b,k;

public:
    Tiesine(float K,float B,float X):b(B),k(K),Funkcija(X)
    {
        cout<<"Sukurta tiesine funkcija\n";
    }

    ~Tiesine(){cout<<"Sunaikinta tiesine funkcija\n";}
    float reiksme(){return k*x+b;}
};

class Laipsine:public Funkcija{
    private:
    float n;
    public:
    Laipsine(float N,float X):n(N),Funkcija(X){cout<<"Sukurta laipsnine funkcija\n";}
    ~Laipsine(){cout<<"Sunaikinta laipsnine funkcija\n";}

    float reiksme(){return pow(x,n);}
};

class Sinusoide:public Funkcija{
public:
    Sinusoide(float X):Funkcija(X){cout<<"Sukurta sinusoidine funkcija\n";}
    ~Sinusoide(){cout<<"Sunaikinta sinusoidine funkcija\n";}

    float reiksme(){return sin(x);}
};

int main(){
    Funkcija *funkc[3];
    int X=5;
    funkc[0]=new Tiesine(1,2,X);
    funkc[1]=new Laipsine(2,X);
    funkc[2]=new Sinusoide(X);
    for(int i=0;i<3;i++){
        cout<<funkc[i]->reiksme()<<endl;
        delete funkc[i];
    }
    return 0;
}