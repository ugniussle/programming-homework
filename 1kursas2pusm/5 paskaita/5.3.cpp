#include <iostream>
#include <string>
using namespace std;
class Prekes{
protected:
    string pavadinimas;
    int kiekis;
    float kaina;
public:
    Prekes(string Pav,int Kiek,float Kaina):pavadinimas(Pav),kiekis(Kiek),kaina(Kaina){};
    virtual float verte(){return kiekis*kaina;}
    virtual void isvesti();

};
void Prekes::isvesti(){
    cout<<"-------------------------\n"<<pavadinimas<<endl
    <<"kiekis: "<<kiekis<<" vnt."<<endl<<"kaina: "<<verte()<<" eur"<<endl;
}
class Prekes_su_nuolaida:public Prekes{
private:
    float nuolaida;
public:
    Prekes_su_nuolaida(string Pav,int Kiek,float Kaina,float Nuolaida):Prekes(Pav,Kiek,Kaina),nuolaida(Nuolaida){};
    float verte(){return kiekis*kaina*(1-nuolaida);}
    void isvesti();
};
void Prekes_su_nuolaida::isvesti(){
    cout<<"-------------------------\n"<<pavadinimas<<endl
    <<"kiekis: "<<kiekis<<" vnt."<<endl<<"kaina: "<<verte()<<" eur"<<endl;
}
int main(){
    Prekes *krepselis[10];
    string s;
    int k,n;
    float kain,nuol;
    cout<<"iveskite prekiu kieki: ";
    cin>>n;
    for(int i=0;i<n;i++){
        cout<<"iveskite prekes pavadinima, kieki, kaina, ir nuolaida (0 jei nera): ";
        cin>>s>>k>>kain>>nuol;
        if(nuol==0)krepselis[i]=new Prekes(s,k,kain);
        else krepselis[i]=new Prekes_su_nuolaida(s,k,kain,nuol);
    }
    for(int i=0;i<n;i++)krepselis[i]->isvesti();
    return 0;
}