#include <iostream>
#include <cmath>
using namespace std;
const float Pi=3.14159;
class Figura{
protected:
    float S,P;
public:
    Figura(){}
    virtual ~Figura(){}
    virtual float plotas()=0;
    virtual float perimetras()=0;
};
class Trikampis:public Figura{
private:
    float a,b,c;
public:
    Trikampis(float A,float B,float C):a(A),b(B),c(C){cout<<"Trikampis sukurtas\n";P=perimetras();S=plotas();}
    ~Trikampis(){cout<<"Trikampis sunaikintas\n";}
    float perimetras(){return a+b+c;}
    float plotas(){
        float pusp=perimetras()/2;
        return sqrt(pusp*(pusp-a)*(pusp-b)*(pusp-c));
    }
};
class Skritulys:public Figura{
private:
    float r;
public:
    Skritulys(float R):r(R){perimetras();plotas();cout<<"Skritulys sukurtas\n";P=perimetras();S=plotas();}
    ~Skritulys(){cout<<"Skritulys sunaikintas\n";}
    float perimetras(){return 2*Pi*r;}
    float plotas(){return Pi*r*r;}
};
class Staciakampis:public Figura{
private:
    float a,b;
public:
    Staciakampis(float A,float B):a(A),b(B){perimetras();plotas();cout<<"Staciakampis sukurtas\n";P=perimetras();S=plotas();}
    ~Staciakampis(){cout<<"Staciakampis sunaikintas\n";}
    float perimetras(){return a*2+b*2;}
    float plotas(){return a*b;}
};
int main(){
    Figura *figuros[3];
    figuros[0]=new Trikampis(2,2,2);
    figuros[1]=new Skritulys(5);
    figuros[2]=new Staciakampis(10,5);
    for(int i=0;i<3;i++){
        cout<<"plotas: "<<figuros[i]->plotas()<<"  Perimetras: "<<figuros[i]->perimetras()<<endl;
        delete figuros[i];
    }
    return 0;
}