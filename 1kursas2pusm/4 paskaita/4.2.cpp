#include <cmath>
#include <iostream>
using namespace std;
const float Pi=3.14159;
class Skritulys{
protected:
    float R;
    float plotas;
public:
    Skritulys(float r):R(r){plotas=R*R*Pi;}
};
class Ritinys:public Skritulys{
protected:
    float H;
public:
    Ritinys(float r,float h):Skritulys(r),H(h){}
    float Turis(){return plotas*H;};
};
int main(){
    Ritinys rit(5,10);
    cout<<rit.Turis()<<endl;
}