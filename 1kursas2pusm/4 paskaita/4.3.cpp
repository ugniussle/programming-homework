#include <iostream>
using namespace std;
static int Count=0;
class Geometrine_figura{
    protected:
    float perimetras;
    float plotas;
    public:
    Geometrine_figura(){Count++;}
    ~Geometrine_figura();
};
Geometrine_figura::~Geometrine_figura(){
    
    cout<<"Figuros perimetras: "<<perimetras<<
    "\nFiguros plotas: "<<plotas<<endl;
    cout<<"--------------------------------------\n";
}
/////////////////////////////////////
class Staciakampis:public Geometrine_figura{
protected:
    float ilgis;
    float plotis;
    public:
    Staciakampis(float l,float l2):ilgis(l),plotis(l2){plotas=ilgis*plotis;perimetras=ilgis*2+plotis*2;}
    Staciakampis();
    ~Staciakampis();
};
Staciakampis::Staciakampis(){
    cout<<"iveskite ilgi ir ploti: ";
    float l,l2;
    cin>>l>>l2;
    ilgis=l;
    plotis=l2;
    plotas=ilgis*plotis;
    perimetras=ilgis*2+plotis*2;
}
Staciakampis::~Staciakampis(){
    cout<<"Figuros ilgis: "<<ilgis<<
    "\nFiguros plotis: "<<plotis<<endl;
}
/////////////////////////////////////
class Staciakampis_gretasienis:public Staciakampis{
protected:
    float aukstis;
    float turis;
    public:
    Staciakampis_gretasienis(float l,float l2,float h);
    Staciakampis_gretasienis();
    ~Staciakampis_gretasienis();
};
Staciakampis_gretasienis::Staciakampis_gretasienis(float l,float l2,float h):Staciakampis(l,l2),aukstis(h){
    turis=aukstis*plotas;
    plotas+=plotas+aukstis*ilgis*2+aukstis*plotis*2;
    perimetras+=perimetras+aukstis*4;
}
Staciakampis_gretasienis::Staciakampis_gretasienis(){
    cout<<"iveskite auksti: ";
    float h;
    cin>>h;
    aukstis=h;
    turis=aukstis*plotas;
    plotas+=plotas+aukstis*ilgis*2+aukstis*plotis*2;
    perimetras+=perimetras+aukstis*4;
}
Staciakampis_gretasienis::~Staciakampis_gretasienis(){
    cout<<"Figura "<<Count<<endl;
    Count--;
    cout<<"Figuros turis: "<<turis<<
    "\nFiguros aukstis: "<<aukstis<<endl;
}

int main(){
    Staciakampis_gretasienis A(1,2,3);
    Staciakampis_gretasienis B;
    return 0;
}