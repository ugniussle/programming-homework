#include <iostream>
#include <cmath>
using namespace std;
class Vekt2d{
protected:
    int X,Y;
public:
    Vekt2d(int x,int y):X(x),Y(y){}
    float ilgis(){return sqrt(X*X+Y*Y);}
};
class Vekt3d:public Vekt2d{
protected:
    int Z;
public:
    Vekt3d(int x,int y,int z):Z(z),Vekt2d(x,y){};
    float ilgis(){return sqrt(X*X+Y*Y+Z*Z);}
};
int main(){
    Vekt3d atk(10,10,10);
    Vekt2d atk2(10,10);
    cout<<atk.ilgis()<<' '<<atk2.ilgis()<<endl;
    system("pause");
    return 0;
}