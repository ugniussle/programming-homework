#include <iostream>
using namespace std;

class Darbuotojas{
private:
	int Amzius;
	int Patirtis;
	int Atlyginimas;
public:
	Darbuotojas();
	Darbuotojas(int amzius,int patirtis,int atlyginimas);
	int GautiAmziu();
	int GautiPatirti();
	int GautiAtlyginima();
	double MokestSodra();
	double MokestPaj();
	
};

Darbuotojas::Darbuotojas(){}
Darbuotojas::Darbuotojas(int amzius,int patirtis,int atlyginimas){
	Amzius=amzius;
	Patirtis=patirtis;
	Atlyginimas=atlyginimas;
}

int Darbuotojas::GautiAmziu(){
	return Amzius;
}

int Darbuotojas::GautiPatirti(){
	return Patirtis;
}

int Darbuotojas::GautiAtlyginima(){
	return Atlyginimas;
}

double Darbuotojas::MokestSodra(){
	return Atlyginimas*0.09;
}

double Darbuotojas::MokestPaj(){
	return Atlyginimas*0.15;
}

int main(){
	Darbuotojas Tomas(23,2,1000);
	cout<<"Amzius: "<<Tomas.GautiAmziu()<<" metai.\nPatirtis: "<<Tomas.GautiPatirti()
	<<" metai.\nAtlyginimas: "<<Tomas.GautiAtlyginima()<<" Eurai.\nSodros mokestis: "
	<<Tomas.MokestSodra()<<" Euru.\nPajamu mokestis: "<<Tomas.MokestPaj()<<" Euru.\n";
	return 0;
}