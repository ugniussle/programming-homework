#include <iostream>
using namespace std;

class Darbuotojas{
private:
	int Amzius;
	int Patirtis;
	int Atlyginimas;

public:
	void IvestiDuom(int amzius,int patirtis,int atlyginimas);
	int GautiAmziu();
	int GautiPatirti();
	int GautiAtlyginima();
	double MokestSodra();
	double MokestPaj();
	
};

void Darbuotojas::IvestiDuom(int amzius,int patirtis,int atlyginimas){
	Amzius=amzius;
	Patirtis=patirtis;
	Atlyginimas=atlyginimas;
}

int Darbuotojas::GautiAmziu(){
	return Amzius;
}

int Darbuotojas::GautiPatirti(){
	return Patirtis;
}

int Darbuotojas::GautiAtlyginima(){
	return Atlyginimas;
}

double Darbuotojas::MokestSodra(){
	return Atlyginimas*0.09;
}

double Darbuotojas::MokestPaj(){
	return Atlyginimas*0.15;
}

int main(){
	Darbuotojas Tomas;
	cout<<"Iveskite darbuotojo amziu, patirti ir atlyginima:";
	int amzius,patirtis,atlyginimas;
	cin>>amzius>>patirtis>>atlyginimas;
	Tomas.IvestiDuom(amzius,patirtis,atlyginimas);
	cout<<"Amzius: "<<Tomas.GautiAmziu()<<" metai.\nPatirtis: "<<Tomas.GautiPatirti()
	<<" metai.\nAtlyginimas: "<<Tomas.GautiAtlyginima()<<" Eurai.\nSodros mokestis: "
	<<Tomas.MokestSodra()<<" Euru.\nPajamu mokestis: "<<Tomas.MokestPaj()<<" Euru.\n";
	return 0;
}