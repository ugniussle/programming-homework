#include <iostream>
#include <cmath>
using namespace std;

class Taskas{
private:
	int X,Y;
public:
	void IvestKoord(int x,int y);
	int GautiX();
	int GautiY();
};

class Atkarpa{
private:
	Taskas Pradzia;
	Taskas Pabaiga;
public:
	void IvestiTaskus();
	double GautiAtkarpa();
};

int Taskas::GautiX(){
	return X;
}

int Taskas::GautiY(){
	return Y;
}

void Taskas::IvestKoord(int x,int y){
	X=x;
	Y=y;
}

void Atkarpa::IvestiTaskus(){
	cout<<"Iveskite pirmo tasko koordinates: ";
	int x,y;
	cin>>x>>y;
	Pradzia.IvestKoord(x,y);
	cout<<"\nIveskite antro tasko koordinates: ";
	cin>>x>>y;
	Pabaiga.IvestKoord(x,y);
}

double Atkarpa::GautiAtkarpa(){
	double atkarpa=sqrt(pow((Pradzia.GautiX()-Pabaiga.GautiX()),2)+
	pow((Pradzia.GautiY()-Pabaiga.GautiY()),2));
	return atkarpa;
}

int main(){
	Atkarpa atk;
	atk.IvestiTaskus();
	cout<<"\nAtstumas tarp tasku: "<<atk.GautiAtkarpa()<<endl;
	return 0;
}